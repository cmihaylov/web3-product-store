const { expect } = require("chai");
const { ethers } = require("hardhat");

var store, owner, anotherAccount;

describe("Store", function () {
  before(async () => {
    // get accounts
    [owner, anotherAccount] = await ethers.getSigners();

    // deploy contract
    const Store = await ethers.getContractFactory("Store");
    store = await Store.deploy();
    await store.deployed();
  });

  it('Should be able to create product with owner', async () => {
    // first check that product does not exist
    let product = await store.products(1);
    expect(product.enabled).to.equal(false);
    expect(product.priceInWei).to.equal(0);
    expect(product.quantityTotal).to.equal(0);
    expect(product.quantitySold).to.equal(0);

    await store.addProduct(1, 500, 100);

    // check after product is created
    product = await store.products(1);
    expect(product.enabled).to.equal(true);
    expect(product.priceInWei).to.equal(500);
    expect(product.quantityTotal).to.equal(100);
    expect(product.quantitySold).to.equal(0);
  });

  it('Should NOT be able to create same product again', async () => {
    await expect(
      store.addProduct(1, 3, 10)
    ).to.be.revertedWith('Product with this ID already exists.');
  });

  it('Should be able to update quantity of existing product', async () => {
    await store.updateQuantity(1, 123);

    // check after product is updated
    let product = await store.products(1);
    expect(product.enabled).to.equal(true);
    expect(product.priceInWei).to.equal(500);
    expect(product.quantityTotal).to.equal(123);
    expect(product.quantitySold).to.equal(0);
  });

  it('Should be able to create product-2 with owner', async () => {
    // first check that product does not exist
    let product = await store.products(2);
    expect(product.enabled).to.equal(false);
    expect(product.priceInWei).to.equal(0);
    expect(product.quantityTotal).to.equal(0);
    expect(product.quantitySold).to.equal(0);

    await store.addProduct(2, 300, 100);

    // check after product is created
    product = await store.products(2);
    expect(product.enabled).to.equal(true);
    expect(product.priceInWei).to.equal(300);
    expect(product.quantityTotal).to.equal(100);
    expect(product.quantitySold).to.equal(0);
  });

  it('Should NOT be able to create product (with account 2)', async () => {
    await expect(
      store.connect(anotherAccount).addProduct(7, 5, 100)
    ).to.be.revertedWith('Ownable: caller is not the owner');
  });

  it('Should fail trying to buy product with not enough Wei (with account 2)', async () => {
    await expect(
      store.connect(anotherAccount).buyProduct(1)
    ).to.be.revertedWith('Sent amount does not match product price.');

    let product = await store.products(1);
    expect(product.quantitySold).to.equal(0);
  });

  it('Should be able to buy product when enough Wei is supplied (with account 2)', async () => {
    await store.connect(anotherAccount).buyProduct(1, { value: 500 });

    let product = await store.products(1);
    expect(product.quantitySold).to.equal(1);

    let purchase = await store.getPurchaseInfo(1, anotherAccount.address);
    expect(purchase.blockNumber).to.be.not.equal(0);
    expect(purchase.returned).to.be.equal(false);
  });

  it('Should NOT be able to buy same product again (with account 2)', async () => {
    await expect(
      store.connect(anotherAccount).buyProduct(1, { value: 500 })
    ).to.be.revertedWith('You have already bought product.');

    let product = await store.products(1);
    expect(product.quantitySold).to.equal(1);
  });

  it('Should get balannce of Store and it should have the money from one sold product', async () => {
    let balance = await store.balance();
    expect(balance).to.be.equal(500);
  });

  it('Should be able to buy product-2 (with account 2)', async () => {
    await store.connect(anotherAccount).buyProduct(2, { value: 300 });

    let product = await store.products(2);
    expect(product.quantitySold).to.equal(1);

    let purchase = await store.getPurchaseInfo(2, anotherAccount.address);
    expect(purchase.blockNumber).to.be.not.equal(0);
    expect(purchase.returned).to.be.equal(false);
  });

  it('Should get balannce of Store and it should have the money from both sold products', async () => {
    let balance = await store.balance();
    expect(balance).to.be.equal(800);
  });

  it('Should be able to return product-2 (with account 2)', async () => {
    await store.connect(anotherAccount).returnProduct(2);

    let product = await store.products(2);
    expect(product.quantitySold).to.equal(0);

    let purchase = await store.getPurchaseInfo(2, anotherAccount.address);
    expect(purchase.returned).to.be.equal(true);
  });

  it('Should get balannce of Store and it should have only money from first sold product', async () => {
    let balance = await store.balance();
    expect(balance).to.be.equal(500);
  });

  it('Should NOT be able to withdraw money (with account 2)', async () => {
    await expect(
      store.connect(anotherAccount).withdraw()
    ).to.be.revertedWith('Ownable: caller is not the owner');

    let balance = await store.balance();
    expect(balance).to.be.equal(500);
  });

  it('Should be able to withdraw money and then balance should be zero (with owner)', async () => {
    await store.withdraw();

    let balance = await store.balance();
    expect(balance).to.be.equal(0);
  });
});
