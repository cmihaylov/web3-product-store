// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.10;

import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract Store is Ownable, ReentrancyGuard {
    struct Purchase {
        uint blockNumber;
        bool returned;
    }
    struct Product {
        bool enabled;
        uint priceInWei;
        uint quantityTotal;
        uint quantitySold;
        mapping(address => Purchase) purchases;
    }
    mapping(uint => Product) public products;
    uint productCount;
    uint highestProductId;

    event ProductCreated(uint id, uint price, uint quantity);
    event ProductQuantityUpdated(uint id, uint quantity);
    event ProductPurchased(uint id, address buyer);
    event ProductReturned(uint id, address buyer);

    modifier productExists(uint _id) {
        require(products[_id].enabled == true, "Product with this ID does not exist.");
        _;
    }

    function addProduct(uint _id, uint _priceInWei, uint _quantity) public onlyOwner {
        require(products[_id].enabled == false, "Product with this ID already exists.");

        products[_id].enabled = true;
        products[_id].priceInWei = _priceInWei;
        products[_id].quantityTotal = _quantity;

        productCount++;
        if (_id > highestProductId) {
            highestProductId = _id;
        }

        emit ProductCreated(_id, _priceInWei, _quantity);
    }

    function updateQuantity(uint _id, uint _quantity) public onlyOwner productExists(_id) {
        products[_id].quantityTotal = _quantity;
        emit ProductQuantityUpdated(_id, _quantity);
    }

    function buyProduct(uint _id) public productExists(_id) nonReentrant payable {
        require(msg.value == products[_id].priceInWei, "Sent amount does not match product price.");
        require(products[_id].quantitySold < products[_id].quantityTotal, "Product is sold out.");
        require(products[_id].purchases[msg.sender].blockNumber == 0 || products[_id].purchases[msg.sender].returned == true, "You have already bought product.");

        products[_id].purchases[msg.sender].blockNumber = block.number;
        products[_id].purchases[msg.sender].returned = false;
        products[_id].quantitySold++;

        emit ProductPurchased(_id, msg.sender);
    }

    function returnProduct(uint _id) public productExists(_id) nonReentrant {
        require(products[_id].purchases[msg.sender].blockNumber > 0, "You have not bought this product, nothing to return.");
        require(products[_id].purchases[msg.sender].returned == false, "You have already returned the product.");
        require(products[_id].purchases[msg.sender].blockNumber + 100 > block.number, "Return policy period expired, cannot refund.");

        products[_id].purchases[msg.sender].returned = true;
        products[_id].quantitySold--;

        (bool sent, ) = msg.sender.call{value: products[_id].priceInWei}("");
        require(sent, "Could not send to refund.");

        emit ProductReturned(_id, msg.sender);
    }

    // gas limits, should probably be implemented off-chain
    function listProducts() public view returns (uint[] memory) {
        uint[] memory memoryArr = new uint[](productCount);
        uint arrayIndex;
        for(uint id = 0; id <= highestProductId; id++) {
            // skip non-existing product ids
            if (products[id].enabled == false) continue;

            memoryArr[arrayIndex] = id;
            arrayIndex++;
        }
        return memoryArr;
    }

    function getPurchaseInfo(uint _productId, address buyer) public view returns (Purchase memory) {
        return products[_productId].purchases[buyer];
    }

    function balance() public view returns (uint) {
        return address(this).balance;
    }

    function withdraw() public onlyOwner {
        (bool sent, ) = owner().call{value: address(this).balance}("");
        require(sent, "Could not withdraw money.");
    }

    receive() external payable {}
}